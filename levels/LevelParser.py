import pygame
import os

from levels.Level import Level
from levels.Victory import Victory

from GameObjects.SoilTile import SoilTile
from GameObjects.GrassTile import GrassTile
from GameObjects.VineTile import VineTile
from GameObjects.Player import Player
from GameObjects.ExitSign import ExitSign
from GameObjects.Cow import Cow
from GameObjects.SeedSack import SeedSack
from GameObjects.Trowel import Trowel
from GameObjects.Seed import Seed
from GameObjects.PortalTile import PortalTile
from GameObjects.GameObject import GameObject
from GameObjects.FireTile import FireTile



###############################################################################
class LevelParser(object):
	'''Parses text files and returns Level objects'''

###############################################################################	
	def __init__(self):
		pass

###############################################################################
	def parseLevel(self, levelNo, currentPlayer):
		levelFile = "levels/Maps/level" + str(levelNo) + ".txt"
		if(os.path.isfile(levelFile)):
			newLevel = Level(levelNo, self)

			currentColumn = 0
			currentRow = 0

			for line in open(levelFile):
				for character in line.lower():
					if(character == 'g'):
						self.addGrass(newLevel, self.getCoords(currentColumn, currentRow))
					elif(character == 's'):
						self.addSoil(newLevel, self.getCoords(currentColumn, currentRow))
					elif(character == 'v'):
						self.addVine(newLevel, self.getCoords(currentColumn, currentRow))
					elif(character == 'p'):
						self.addPlayer(newLevel, self.getCoords(currentColumn, currentRow), currentPlayer)
					elif(character == 'o'):
						self.addExit(newLevel, self.getCoords(currentColumn, currentRow))
					elif(character == '1'):
						self.addSeedSack(newLevel, self.getCoords(currentColumn, currentRow))
					elif(character == '2'):
						self.addTrowel(newLevel, self.getCoords(currentColumn, currentRow))
					elif(character == 'c'):
						self.addCow(newLevel, self.getCoords(currentColumn, currentRow))
					elif(character == '+'):
						self.addVineSeed(newLevel, self.getCoords(currentColumn, currentRow))
					elif(character == 't'):
						self.addPortal(newLevel, self.getCoords(currentColumn, currentRow), GameObject.right)
					elif(character == 'r'):
						self.addPortal(newLevel, self.getCoords(currentColumn, currentRow), GameObject.left)
					elif(character == "*"):
						self.addPortalSeed(newLevel, self.getCoords(currentColumn, currentRow))
					elif(character == "f"):
						self.addFirePlant(newLevel, self.getCoords(currentColumn, currentRow))
					currentColumn += 1
				
				currentRow += 1
				currentColumn = 0

			return newLevel
		else:
			return Victory()

###############################################################################
	def addSoil(self, level, coords):
		level.addGround(SoilTile(coords))

###############################################################################
	def addGrass(self, level, coords):
		level.addGround(GrassTile(coords))

###############################################################################
	def addVine(self, level, coords):
		level.addPlant(VineTile(coords, False))

###############################################################################
	def addPlayer(self, level, coords, currentPlayer):
		##how do we get the player from one level to the next? 
		if(currentPlayer == None):
			level.addPlayer(Player(coords))
			#TODO REALLY ALSO GET RID OF THIS
			#level.player.hasTrowel = True
			#level.player.hasSeedSack = True
		else:
			currentPlayer.position.topleft = coords
			level.addPlayer(currentPlayer)

###############################################################################
	def addExit(self, level, coords):
		level.addExit(ExitSign(coords))

###############################################################################
	def addSeedSack(self, level, coords):
		level.addUpgrade(SeedSack(coords))

###############################################################################
	def addTrowel(self, level, coords):
		level.addUpgrade(Trowel(coords))

###############################################################################
	def addCow(self, level, coords):
		level.addCow(Cow(coords))

###############################################################################
	def addVineSeed(self, level, coords):
		level.addSeed(coords, Seed.vine) 

###############################################################################
	def addPortalSeed(self, level, coords):
		level.addSeed(coords, Seed.portal)

###############################################################################
	def addPortal(self, level, coords, direction):
		level.addPlant(PortalTile(coords, False, direction))

###############################################################################
	def addFirePlant(self, level, coords):
		level.addPlant(FireTile(coords, False, GameObject.left))

###############################################################################
	def getCoords(self, row, column):
		return (row * 25, column * 25)
