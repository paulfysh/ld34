import pygame
import Globals
import sys

from GameObjects.GameScreen import GameScreen

class Victory(GameScreen):

###############################################################################
	def __init__(self):
		super(Victory, self).__init__()
		self.font = pygame.font.SysFont("monospace", 15)

		self.renderLabel = self.font.render("You reached your garden with your tools! Press escape to quit", 1, pygame.Color("Red"))
		self.labelRect = self.renderLabel.get_rect()
		self.labelRect.center = (Globals.screenSize[0] / 2, Globals.screenSize[1] / 2)

###############################################################################
	def draw(self):
		Globals.screen.blit(self.renderLabel, self.labelRect)

###############################################################################
	def handleEvent(self, event):
		if(event.type == pygame.KEYDOWN):
			if(event.key == pygame.K_ESCAPE):
				sys.exit()