import pygame
import Globals
import itertools
import copy
import math

from GameObjects.Seed import Seed
from GameObjects.GameScreen import GameScreen
from menus.InGameMenu import InGameMenu

class Level(GameScreen):

###############################################################################
	def __init__(self, levelNo, levelParser):
		super(Level, self).__init__()
		self.parser = levelParser
		self.cows = []
		self.player = None
		#self.objective = None
		self.plants = []
		self.exit = None
		self.upgrades = []
		self.seeds = []
		self.fireballs = []

		self.groundTiles = []

		self.damageSound = pygame.mixer.Sound("assets/sounds/damage.wav")
		self.damageChannel = None

		self.background = pygame.image.load("assets/sprites/background.png").convert_alpha()
		self.backgroundRect = self.background.get_rect().topleft = (0,0)

		self.levelNo = levelNo

		self.fallingSpeed = 500
		self.maxSurvivableFall = 170
		self.lastTick = Globals.currentTime

		if(self.levelNo == 1):
			self.introSpeech = pygame.mixer.Sound("assets/sounds/Intro.wav")
			Globals.currentlyPlaying = self.introSpeech
			Globals.playSpeechEffect(self.introSpeech, Globals.currentlyPlaying)
		elif(self.levelNo == 3):
			self.cowSpeech = pygame.mixer.Sound("assets/sounds/CowSpeech.wav")
			Globals.playSpeechEffect(self.cowSpeech, Globals.currentlyPlaying)
		elif(self.levelNo == 4):
			self.portalSpeech = pygame.mixer.Sound("assets/sounds/PortalSpeech.wav")
			Globals.playSpeechEffect(self.portalSpeech, Globals.currentlyPlaying)
		elif(self.levelNo == 6):
			self.fireplantSpeech = pygame.mixer.Sound("assets/sounds/fireplantSpeech.wav")
			Globals.playSpeechEffect(self.fireplantSpeech, Globals.currentlyPlaying)

###############################################################################
	def addPlayer(self, player):
		self.player = player
		self.player.setPlantCallback(self.addPlant)

###############################################################################
	def addCow(self, cow):
		self.cows.append(cow)

###############################################################################
	def addUpgrade(self, upgrade):
		self.upgrades.append(upgrade)

###############################################################################
	def addGround(self, ground):
		self.groundTiles.append(ground)

###############################################################################
	def addPlant(self, plant):
		if((type(plant).__name__ == "PortalTile")):
			self.addPortal(plant)
		elif((type(plant).__name__ == "FireTile")):
			self.addFireTile(plant)

		#make sure the plant is in a sensible place
		tempPos = copy.deepcopy(plant.position)
		tempPos.bottom += 5 #force it to collide with the ground tiles
		collisions = self.simpleGetGroundTilesUnder(tempPos)

		newPosition = copy.deepcopy(plant.position)
		newLeft = None
		moved = False
		for collision in collisions:
			if(collision.position.bottom == newPosition.bottom):
				if(collision.position.left < newPosition.left):
					newPosition.left = collision.position.right
					moved = True
				if(collision.position.right > newPosition.right):
					newPosition.right = collision.position.left
					moved = True
			else:
				newLeft = collision.position.left
			
		if(not moved and newLeft):
			newPosition.left = collision.position.left

		plant.move(newPosition)
		self.plants.append(plant)

###############################################################################
	def addPortal(self, portal):
		#there can only ever be 2 portals on a level. 
		for plant in self.plants:
			if((type(plant).__name__ == "PortalTile")):
				plant.linkPortals(portal)
				portal.linkPortals(plant)

		portal.addCollisionCallback(self.simpleGetGroundTilesUnder)

###############################################################################
	def addFireTile(self, firetile):
		firetile.setFireballCreationCallback(self.addFireball)

###############################################################################
	def addFireball(self, fireball):
		self.fireballs.append(fireball)


###############################################################################
	def addExit(self, exit):
		self.exit = exit

###############################################################################
	def draw(self):
		Globals.screen.blit(self.background, self.backgroundRect)

		for ground in self.groundTiles:
			ground.draw()

		for plant in self.plants:
			plant.draw()

		for upgrade in self.upgrades:
			upgrade.draw()

		for cow in self.cows:
			cow.draw()

		for seed in self.seeds:
			seed.draw()

		for fireball in self.fireballs:
			fireball.draw()

		self.exit.draw()
		self.player.draw()



###############################################################################
	def update(self):
		self.handleFalling(self.player, True, False)
		self.player.update(self.checkCollision)
		self.checkForUpgradeGrab()
		self.checkForLevelComplete()
		self.checkForSeedGrab()

		removeList = []
		for plant in self.plants:
			plant.update(self.simpleGetGroundTilesUnder, self.addPlant)
			if(plant.destroyed):
				removeList.append(plant)
				if(plant.createSeed):
					self.addSeed(plant.position.topleft, plant.seedtype)

		for item in removeList:
			self.plants.remove(item)

		for cow in self.cows:
			self.handleFalling(cow, False, True)
			cow.update(self.simpleGetCollision)
			#cows are not ninjas, they cannot eat while falling
			if(cow.startedFallingAt == None):
				self.checkForCowEatingPlant(cow)

		for fireball in self.fireballs:
			fireball.update()
			self.checkFireBallHitSomething(fireball)


		self.lastTick = Globals.currentTime

###############################################################################
	def addSeed(self, position, type):
		self.seeds.append(Seed(position, type))


###############################################################################
	def handleFalling(self, faller, allowVines, useSimple):
		distance = self.fallingSpeed * (Globals.currentTime - self.lastTick)

		#should look to tidy this, Im doing the same collision detection a
		#bunch of times.
		hit = self.shouldFall(faller, distance, useSimple, allowVines)
		if(not hit):
			if(faller.startedFallingAt == None):
				faller.startedFallingAt = copy.deepcopy(faller.position)
			
			newPosition = copy.deepcopy(faller.position)
			newPosition.bottom += distance


			if(useSimple):
				collidesWith = self.simpleGetCollision(newPosition)
			else:
				collidesWith = self.checkCollision(newPosition, faller.image)
			collided = False
			for item in collidesWith:
				if( (type(item).__name__ == "GrassTile") or 
				(type(item).__name__ == "SoilTile") or 
				(allowVines and type(item).__name__ == "VineTile")):
					collided = True
					faller.position.bottom = item.position.top

			if(not collided):
				faller.position.bottom += distance


		elif(not faller.startedFallingAt == None):
			if(self.fallWasLethal(faller)):
				if(not self.damageChannel or not self.damageChannel.get_busy()):
					self.damageChannel = self.damageSound.play()
				faller.kill()
			faller.position.bottom = hit.position.top
			faller.startedFallingAt = None
		elif(hit and faller.position.bottom != hit.position.top):
			if(hit.position.top > faller.position.bottom):
				if(type(hit).__name__ != "VineTile"):
					faller.position.bottom = hit.position.top

###############################################################################
	def playerPrint(self, item, msg):
		if(type(item).__name__ == "Player"):
			print(msg)

	def cowPrint(self, item, msg):
		if(type(item).__name__ == "Cow"):
			print(msg)

###############################################################################
	def shouldFall(self, faller, distance, useSimple, allowVines):
		newPosition = copy.deepcopy(faller.position)
		newPosition.top += 5
		if(useSimple):
			willCollideWith = self.simpleGetCollision(newPosition)
		else:
			willCollideWith = self.checkCollision(newPosition, faller.image)

		for collision in willCollideWith:
			if(type(collision).__name__ == "GrassTile" or 
			type(collision).__name__ == "SoilTile" or 
			(allowVines and type(collision).__name__ == "VineTile")):

				#if(not useSimple or (collision.position.top <= newPosition.bottom)):
				return collision

		return None

###############################################################################
	def fallWasLethal(self, faller):
		xVal = faller.startedFallingAt.left - faller.position.left
		yVal = faller.startedFallingAt.top - faller.position.top

		xSquare = math.pow(xVal, 2)
		ySquare = math.pow(yVal, 2)

		distance = math.sqrt(xSquare + ySquare)

		if(distance >= self.maxSurvivableFall):
			return True

		return False

###############################################################################
	def handleEvent(self, event):
		if(event.type == pygame.KEYDOWN):
			if(event.key == pygame.K_ESCAPE):
				self.nextScreen = InGameMenu(self, self.parser)
		self.player.handleEvent(event)

###############################################################################
	def checkCollision(self, rectToCheck, image):
		'''callback function, player calls on this to see if it is colliding with any
		objects in the level and get them back, also checks for victory condition'''
		collisions = []

		for item in itertools.chain(self.plants):
			if(rectToCheck.colliderect(item.position)):
				if(not item.growing):
					collisions.append(item)

		for item in self.groundTiles:
			if(rectToCheck.colliderect(item.position)):
				offset_x = item.position[0] - rectToCheck[0]
				offset_y = item.position[1] - rectToCheck[1]
				if(pygame.mask.from_surface(image).overlap(pygame.mask.from_surface(item.image), (offset_x, offset_y))):
					collisions.append(item)


		return collisions

###############################################################################
	def simpleGetGroundTilesUnder(self, rect):
		collisions = []

		for item in self.groundTiles:
			if(item.position.colliderect(rect)):
				collisions.append(item)

		return collisions

###############################################################################
	def simpleGetCollision(self, rect):
		collisions = []

		for item in itertools.chain(self.plants, self.groundTiles):
			if(rect.colliderect(item.position)):
				collisions.append(item)

		return collisions


###############################################################################
	def checkForUpgradeGrab(self):
		remove = None
		for upgrade in self.upgrades:
			if(self.player.position.colliderect(upgrade.position)):
				self.player.getUpgrade(upgrade)
				remove = upgrade
		
		if(remove):
			self.upgrades.remove(remove)

###############################################################################
	def checkForSeedGrab(self):
		remove = None
		for seed in self.seeds:
			if(self.player.position.colliderect(seed.position)):
				self.player.getSeed(seed)
				remove = seed
		if(remove):
			self.seeds.remove(remove)

###############################################################################
	def checkForLevelComplete(self):
		if(self.player.position.colliderect(self.exit.position)):
			#player won the level!	
			self.player.resetForNextLevel()
			self.nextScreen = self.parser.parseLevel(self.levelNo + 1, self.player)

			if(self.levelNo == 1):
				Globals.achivements.unlockGreenFinger()
			elif(self.levelNo == 4):
				Globals.achivements.unlockThinkingWithPortals()
			elif(self.levelNo == 7):
				Globals.achivements.unlockNaturalBornGrower()
				if(Globals.achivements.cowsKilled == 0):
					Globals.achivements.unlockPacifist()

###############################################################################
	def checkForCowEatingPlant(self, cow):
		if(not cow.dead):
			plantsToEat = []
			for plant in self.plants:
				if(cow.position.colliderect(plant.position)):
					offset_x = plant.position[0] - cow.position[0]
					offset_y = plant.position[1] - cow.position[1] 
					if(pygame.mask.from_surface(cow.image).overlap(pygame.mask.from_surface(plant.image), (offset_x, offset_y))):
						if(type(plant).__name__ == "PortalTile"):
							if(not plant.growing):
								if(Globals.currentTime > cow.lastPortaled + cow.portalCooldown):
									cow.lastPortaled = Globals.currentTime
									plant.moveToOtherPortal(cow)
									if(self.shouldFall(cow, 1, True, False) == None):
										Globals.achivements.unlockCowTipping()
						else:
							addPlant = True
							for knownPlant in plantsToEat:
								if(knownPlant.position.left == plant.position.left):
									if(plant.position.top > knownPlant.position.top):
										plantsToEat.remove(knownPlant)
									else:
										addPlant = False
							if(addPlant):
								plantsToEat.append(plant)
								
			
			for plant in plantsToEat:
				cow.eatPlant(plant)

###############################################################################
	def checkFireBallHitSomething(self, fireball):
		fireballsToRemove = []
		if(not fireball.destroyed):
			for item in self.cows:
				if(item.position.colliderect(fireball.position)):
					item.kill()
					fireballsToRemove.append(fireball)

			for item in self.groundTiles:
				if(item.position.colliderect(fireball.position)):
					fireballsToRemove.append(fireball)

			for plant in self.plants:
				if(type(plant).__name__ == "PortalTile"):
					if(plant.position.colliderect(fireball.position)):
						if(Globals.currentTime > fireball.lastPortaled + fireball.portalCooldown):
								fireball.lastPortaled = Globals.currentTime
								if(plant.moveToOtherPortal(fireball)):
									fireball.setDirectionModifider(plant.getExitPortalFacing())
				elif(type(plant).__name__ == "FireTile"):
					if(plant.position.colliderect(fireball.position)):
						if(not plant is fireball.creator and not plant.dead):
							plant.destroy(False, True)
							fireballsToRemove.append(fireball)


			#since its a player, probably want to do a detailed check
			if(self.player.position.colliderect(fireball.position)):
				offset_x = fireball.position[0] - self.player.position[0]
				offset_y = fireball.position[1] - self.player.position[1]
				if(pygame.mask.from_surface(self.player.image).overlap(pygame.mask.from_surface(fireball.image), (offset_x, offset_y))):
					self.player.kill()
					fireballsToRemove.append(fireball)

			if(fireball.position.left < 0 or fireball.position.left > Globals.screenSize[0]):
				fireballsToRemove.append(fireball)

			for ball in fireballsToRemove:
				if(ball in self.fireballs):
					ball.destroy()
					if(not self.damageChannel or not self.damageChannel.get_busy()):
						self.damageChannel = self.damageSound.play()
					
					self.fireballs.remove(ball)


