# -*- mode: python -*-
a = Analysis(['main.py'],
             pathex=['c:\\Users\\Paul\\Documents\\ld34'],
             hiddenimports=[],
             hookspath=None,
             runtime_hooks=None)
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='Greenfinger.exe',
          debug=False,
          strip=None,
          upx=True,
          console=True )
image_tree = Tree('assets', prefix='assets')
level_tree = Tree('levels/Maps', prefix='levels/Maps')
coll = COLLECT(image_tree,
	       level_tree,
	       exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=None,
               upx=True,
               name='Greenfinger')