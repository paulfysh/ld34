import pygame
import sys
import time
import Globals
from ScreenManager import ScreenManager
from Achivements import Achivements


pygame.mixer.pre_init(44100, -16, 1, 512)
pygame.init()

size = width, height = 1024, 768
screen = pygame.display.set_mode(size)
clock = pygame.time.Clock()

Globals.screenSize = size
Globals.screen = pygame.display.get_surface()
Globals.currentTime = time.time()

Globals.mixer = pygame.mixer.init()
Globals.background = pygame.mixer.music.load("assets/sounds/background.mid")
Globals.playSpeech = True
Globals.currentlyPlaying = None
pygame.mixer.music.play(loops=-1)

pygame.display.set_caption('GreenFinger')

screenMgr = ScreenManager()

font = pygame.font.SysFont("monospace", 12)

Globals.achivements = Achivements()

while True:
	Globals.currentTime = time.time()
	screen.fill(pygame.Color("Black"))
	Globals.fps = clock.get_fps()

	for event in pygame.event.get():
		if(event.type == pygame.QUIT):
			pygame.quit()
			sys.exit()
		elif(event.type == pygame.KEYDOWN):
			if(event.key == pygame.K_m):
				if(pygame.mixer.music.get_busy()):
					pygame.mixer.music.stop()
				else:
					pygame.mixer.music.play()
			if(event.key == pygame.K_n):
				Globals.playSpeech = not Globals.playSpeech

		screenMgr.handleEvent(event)

	screenMgr.update()
	screenMgr.draw()

	Globals.achivements.draw()

	renderLabel = font.render("FPS: " + str(clock.get_fps()), 1, pygame.Color("Red"))
	labelRect = renderLabel.get_rect()
	labelRect.topleft = (0,0)
	screen.blit(renderLabel, labelRect)

	pygame.display.flip()
	clock.tick(60)