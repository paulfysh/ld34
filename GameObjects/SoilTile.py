import pygame

from GameObjects.GameObject import GameObject


class SoilTile(GameObject):
###############################################################################
	def __init__(self, position):
		self.image = pygame.image.load("assets/sprites/soilTile.png").convert_alpha()
		self.position = self.image.get_rect()
		self.position.topleft = position
		super(SoilTile, self).__init__(self.image, self.position)