import pygame

from GameObjects.GameObject import GameObject


class GrassTile(GameObject):
###############################################################################
	def __init__(self, position):
		self.image = pygame.image.load("assets/sprites/grassTile.png").convert_alpha()
		self.position = self.image.get_rect()
		self.position.topleft = position
		super(GrassTile, self).__init__(self.image, self.position)