import pygame
import Globals
import copy

from GameObjects.GameObject import GameObject

class Plant(GameObject):
###############################################################################
	def __init__(self):
		super(Plant, self).__init__(self.image, self.position)

		self.growing = True
		self.timeToGrow = 1
		self.currentPercentage = 0.0
		self.startedGrowing = Globals.currentTime
		self.createAnother = False
		self.destroyed = False
		self.createSeed = False
		self.seedType = None


###############################################################################
	def draw(self):
		if(not self.growing):
			super(Plant,self).draw()
		else:
			growthRect = pygame.Rect(0, 0, 0, 0)
			heightToShow = self.position.height * self.currentPercentage
			growthRect = pygame.Rect(0, self.position.height - heightToShow, self.position.width, heightToShow)

			currentPos = copy.deepcopy(self.position)
			currentPos.top += self.position.height - heightToShow
			Globals.screen.blit(self.image, currentPos, growthRect)

###############################################################################
	def update(self, collisionCallback, plantCallback):
		if(self.startedGrowing != Globals.currentTime):
			self.currentPercentage = self.timeToGrow * (Globals.currentTime - self.startedGrowing)
			if(self.currentPercentage >= 1):
				self.currentPercentage = 1
				self.growing = False

###############################################################################
	def move(self, newPos):
		self.position = newPos

###############################################################################
	def destroy(self, createSeed):
		self.destroyed = True
		self.growing = False
		if(createSeed):
			self.createSeed = True