import pygame
import Globals
import copy

from GameObjects.GameObject import GameObject
from GameObjects.Plant import Plant
from GameObjects.Seed import Seed


class PortalTile(Plant):
###############################################################################
	def __init__(self, position, growing, facing):
		self.image = pygame.image.load("assets/sprites/portal.png").convert_alpha()
		self.position = self.image.get_rect()
		self.position.topleft = position
		super(PortalTile, self).__init__()

		self.linkedPortal = None
		self.seedtype = Seed.portal
		self.facing = facing
		self.growing = growing

		self.portalSound = pygame.mixer.Sound("assets/sounds/portal.wav")
		self.portalChannel = None

		self.collisionCallback = None

###############################################################################
	def linkPortals(self, otherPortal):
		self.linkedPortal = otherPortal

###############################################################################
	def addCollisionCallback(self, callback):
		self.collisionCallback = callback

###############################################################################
	def moveToOtherPortal(self, movingObject):
		portaled = False
		if(self.linkedPortal and not self.growing and not self.destroyed):
			tempPosition = copy.deepcopy(movingObject.position)

			if(self.linkedPortal.facing == GameObject.left):
				tempPosition.bottomleft = self.linkedPortal.position.bottomleft
				tempPosition.left -= movingObject.position.width
			else:
				tempPosition.bottomleft = self.linkedPortal.position.bottomright

			collisions = self.collisionCallback(tempPosition)

			if(len(collisions) == 0):
				movingObject.position = tempPosition
				portaled = True
				if(not self.portalChannel or not self.portalChannel.get_busy()):
					self.portalSound.play()
		return portaled


###############################################################################
	def getExitPortalFacing(self):
		return self.linkedPortal.facing
###############################################################################
	def destroy(self, createSeed):
		super(PortalTile, self).destroy(createSeed)
		if(self.linkedPortal):
			self.linkedPortal.linkedPortal = None
