import pygame

from GameObjects.GameObject import GameObject


class ExitSign(GameObject):
###############################################################################
	def __init__(self, position):
		self.image = pygame.image.load("assets/sprites/WorkSign.png").convert_alpha()
		self.position = self.image.get_rect()
		self.position.topleft = position
		super(ExitSign, self).__init__(self.image, self.position)