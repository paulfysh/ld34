import pygame

from GameObjects.GameObject import GameObject


class Seed(GameObject):
	vine = 0
	portal = 1
	fire = 2

###############################################################################
	def __init__(self, position, seedtype):
		if(seedtype == Seed.vine):
			self.image = pygame.image.load("assets/sprites/seed.png").convert_alpha()
		elif(seedtype == Seed.portal):
			self.image = pygame.image.load("assets/sprites/portalseed.png").convert_alpha()
		else:
			self.image = pygame.image.load("assets/sprites/fireseed.png").convert_alpha()
		
		self.position = self.image.get_rect()
		self.position.topleft = position
		self.seedtype = seedtype
		super(Seed, self).__init__(self.image, self.position)