import pygame
import Globals
import copy

from GameObjects.GameObject import GameObject
from GameObjects.Plant import Plant
from GameObjects.Seed import Seed
from GameObjects.Fireball import Fireball


class FireTile(Plant):

###############################################################################
	def __init__(self, position, growing, facing):
		self.openImage = pygame.image.load("assets/sprites/firePlantOpen.png").convert_alpha()
		self.closedImage = pygame.image.load("assets/sprites/firePlant.png").convert_alpha()
		self.deadImage = pygame.image.load("assets/sprites/firePlantDead.png").convert_alpha()

		if(facing == GameObject.left):
			self.openImage = pygame.transform.flip(self.openImage, True, False)
			self.closedImage = pygame.transform.flip(self.closedImage, True, False)

		self.image = self.closedImage
		
		self.position = self.image.get_rect()
		self.position.topleft = position
		super(FireTile, self).__init__()

		self.seedtype = Seed.fire
		self.facing = facing
		self.growing = growing

		self.lastFired = Globals.currentTime
		self.timeBetweenShots = 2.0
		#self.nextImageTransition = self.lastFired + (self.timeBetweenShots / 2)


		self.fireballCallback = None
		self.dead = False

###############################################################################
	def destroy(self, createSeed, switchToDeadImage=False):
		super(FireTile, self).destroy(createSeed)
		
		if(switchToDeadImage):
			self.destroyed = False
			self.image = self.deadImage
			self.dead = True
###############################################################################
	def setFireballCreationCallback(self, callback):
		self.fireballCallback = callback

###############################################################################
	def update(self, collisionCallback, plantCallback):
		super(FireTile, self).update(collisionCallback, plantCallback)
		if(not self.growing and not self.destroyed and not self.dead):
			if(Globals.currentTime > (self.lastFired + self.timeBetweenShots * 0.25)):
				self.image = self.closedImage
			if(Globals.currentTime > (self.lastFired + self.timeBetweenShots * 0.75)):
				self.image = self.openImage

			#	self.nextImageTransition = Globals.currentTime + self.timeBetweenShots
			if(Globals.currentTime > self.lastFired + self.timeBetweenShots):
				modifier = 1
				position = self.position.topright
				#position = (position[0] + 25, position[1]) #hack because time
				if(self.facing == GameObject.left):
					modifier = -1
					position = self.position.topleft 
				#	position = (position[0] - 25, position[1])
				
				self.fireballCallback(Fireball(position, modifier, self))
				self.lastFired = Globals.currentTime


				#should offset it by half a second?
				