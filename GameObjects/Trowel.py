import pygame

from GameObjects.GameObject import GameObject


class Trowel(GameObject):
###############################################################################
	def __init__(self, position):
		self.image = pygame.image.load("assets/sprites/Trowel.png").convert_alpha()
		self.position = self.image.get_rect()
		self.position.topleft = position
		super(Trowel, self).__init__(self.image, self.position)