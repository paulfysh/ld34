import pygame
import Globals
import copy

from GameObjects.GameObject import GameObject
from GameObjects.Plant import Plant
from GameObjects.Seed import Seed


class VineTile(Plant):
###############################################################################
	def __init__(self, position, growing):
		self.image = pygame.image.load("assets/sprites/vines.png").convert_alpha()
		self.position = self.image.get_rect()
		self.position.topleft = position
		super(VineTile, self).__init__()

		self.createAnother = True
		self.destroyed = False
		self.growing = growing
		self.seedtype = Seed.vine

		self.childPlants = []


###############################################################################
	def destroy(self, createSeed):
		super(VineTile, self).destroy(createSeed)

		for child in self.childPlants:
			child.destroy(False)

###############################################################################
	def update(self, collisionCallback, plantCallback):
		if(self.growing):
			if(self.currentPercentage == 0):
				checkRect = copy.deepcopy(self.position)
				checkRect.top -= 25	
				if(checkRect.top <= 0):
					self.createAnother = False
				else:
					collisions = collisionCallback(checkRect)
					if(len(collisions) > 0):
						self.createAnother = False
			
			if(self.startedGrowing != Globals.currentTime):
				self.currentPercentage = self.timeToGrow * (Globals.currentTime - self.startedGrowing)
				if(self.currentPercentage >= 1):
					self.currentPercentage = 1
					self.growing = False

					newPlantPos = copy.deepcopy(self.position)
					newPlantPos.bottom = self.position.top
					if(self.createAnother):
						newPlant = VineTile(newPlantPos.topleft, True)
						self.childPlants.append(newPlant)
						plantCallback(newPlant)

###############################################################################	
	def draw(self):
		super(VineTile, self).draw()

###############################################################################	
	#def draw(self):
#		if(not self.growing):
#			super(VineTile,self).draw()
#		else:
#			growthRect = pygame.Rect(0, 0, 0, 0)
#			heightToShow = self.position.height * self.currentPercentage
#			growthRect = pygame.Rect(0, self.position.height - heightToShow, self.position.width, heightToShow)
#
#			currentPos = copy.deepcopy(self.position)
#			currentPos.top += self.position.height - heightToShow
#			Globals.screen.blit(self.image, currentPos, growthRect)


 		


