import pygame
import Globals
import itertools
import copy
import math

from GameObjects.GameObject import GameObject
from GameObjects.VineTile import VineTile
from GameObjects.PortalTile import PortalTile
from GameObjects.Seed import Seed

class Player(GameObject):
###############################################################################
	def __init__(self, position):
		self.walkingAnimation = []
		self.walkingAnimation.append(pygame.image.load("assets/sprites/Gardener.png").convert_alpha())
		self.walkingAnimation.append(pygame.image.load("assets/sprites/GardenerWalk1.png").convert_alpha())
		self.hasItemImage = pygame.image.load("assets/sprites/GardenerHasItem.png").convert_alpha()

		self.digSound = pygame.mixer.Sound("assets/sounds/digging.wav")
		self.climbSound = pygame.mixer.Sound("assets/sounds/climb.wav")
		self.climbChannel = None
		self.plantingSound = pygame.mixer.Sound("assets/sounds/planting.wav")
		self.powerUpSound = pygame.mixer.Sound("assets/sounds/powerup.wav")
		self.pickupSeedSound = pygame.mixer.Sound("assets/sounds/pickupSeed.wav")

		self.seedSackSpeech = pygame.mixer.Sound("assets/sounds/seedSackSpeech.wav")
		self.trowelSpeech = pygame.mixer.Sound("assets/sounds/trowelSpeech.wav")

		self.image = self.walkingAnimation[0]

		self.position = self.image.get_rect()
		self.position.topleft = position

		self.font = pygame.font.SysFont("monospace", 14)

		super(Player, self).__init__(self.image, self.position)

		self.speedPerSecond = 200
		self.lastTick = Globals.currentTime

		self.fallingSpeed = 500
		self.maxSurvivableFall = 300
		self.startedFallingAt = None

		self.timeBetweenFrames = 0.2
		self.lastTransition = Globals.currentTime
		self.animationPos = 0
		self.imageFlipped = False

		#player upgrades
		self.upgradeImageDuration = 0.5 #hopefully this gets replaced with a sound effect duration
		self.upgradeImageStarted = None
		self.upgradeImage = None
		self.upgradeImagePos = None
		self.hasSeedSack = False
		self.handleGetItemImageFinished = True
		self.ableToPlant = True

		self.hasTrowel = False
		self.ableToHarvest = True

		#seed Counts
		self.plantCallback = None
		self.numVines = 0
		self.vinesThumbnail = pygame.image.load("assets/sprites/vines.png").convert_alpha()
		self.vinesThumbnailPos = self.vinesThumbnail.get_rect()

		self.numPortals = 0
		self.portalsThumbnail = pygame.image.load("assets/sprites/portal.png").convert_alpha()
		self.portalsThumbnailPos = self.portalsThumbnail.get_rect()

		self.selectedSeed = 0
		self.numSeedTypes = 2
		self.ableToChangeSeed = True

		self.direction = GameObject.right

		self.dead = False

###############################################################################
	def resetForNextLevel(self):
		self.numVines = 0
		self.numPortals = 0

		self.ableToHarvest = True
		self.ableToPlant = True

		self.handleGetItemImageFinished = True

		self.dead = False


###############################################################################
	def update(self, collisionCallback):
		#change walking sprite and the like
		moving = False

		#self.handleFalling(collisionCallback)
		if(not self.handleGetItemImageFinished):
			if(Globals.currentTime > self.upgradeImageDuration + self.upgradeImageStarted):
				self.upgradeImage = None
				self.upgradeImagePos = None
				self.handleGetItemImageFinished = True
		else:
			moving = self.handleKeyboard(collisionCallback)
			self.handleMouse(collisionCallback)

			#stop him being stationary on the walking frame
			if(not moving and not self.dead):
				self.animationPos = 0
				if(self.imageFlipped):
					self.image = pygame.transform.flip(self.walkingAnimation[0], True, False)
				else:
					self.image = self.walkingAnimation[0]

		self.lastTick = Globals.currentTime

###############################################################################
	def handleKeyboard(self, collisionCallback):
		if(not self.dead):
			moving = False
			pressed = pygame.key.get_pressed()
			if(pressed[pygame.K_w] or pressed[pygame.K_UP]):
				self.moveUp(collisionCallback)
				moving = True
			if(pressed[pygame.K_s] or pressed[pygame.K_DOWN]):
				self.moveDown(collisionCallback)
				moving = True
			if(pressed[pygame.K_a] or pressed[pygame.K_LEFT]):
				self.moveLeft(collisionCallback)
				moving = True
			if(pressed[pygame.K_a] or pressed[pygame.K_RIGHT]):
				self.moveRight(collisionCallback)
				moving = True
			if(pressed[pygame.K_SPACE]):
				self.plantSeed()
			if(pressed[pygame.K_LCTRL]):
				self.harvestPlant(collisionCallback)
			if(pressed[pygame.K_q]):
				if(self.ableToChangeSeed):
					self.selectedSeed += 1
					if(self.selectedSeed >= self.numSeedTypes):
						self.selectedSeed = 0
					self.ableToChangeSeed = False
			if(pressed[pygame.K_e]):
				if(self.ableToChangeSeed):
					self.selectedSeed -= 1
					if(self.selectedSeed < 0):
						self.selectedSeed = self.numSeedTypes - 1
					self.ableToChangeSeed = False
			return moving
		else:
			return False

###############################################################################
	def handleMouse(self, collisionCallback):
		if(not self.dead):
			clicked = pygame.mouse.get_pressed()
			if(clicked[0]):
				self.plantSeed()
			elif(clicked[2]):
				self.harvestPlant(collisionCallback)

###############################################################################
	def plantSeed(self):
		if(self.hasSeedSack and self.ableToPlant):
			if(self.selectedSeed == 0):
				if(self.numVines > 0):
					self.numVines -= 1
					self.plantingSound.play()
					self.plantCallback(VineTile(self.position.topleft, True))
			elif(self.selectedSeed == 1):
				if(self.numPortals > 0 and not self.direction == None):
					self.numPortals -= 1
					self.plantingSound.play()
					self.plantCallback(PortalTile(self.position.topleft, True, self.direction))
		
		self.ableToPlant = False

###############################################################################
	def harvestPlant(self, collisionCallback):
		if(self.hasTrowel and self.ableToHarvest):
			collisions = collisionCallback(self.position, self.image)
			for collision in collisions:
				if (type(collision).__name__ == "VineTile"):
					collision.destroy(True)
					self.digSound.play()
					break
				if(type(collision).__name__ == "PortalTile"):
					collision.destroy(True)
					break
		
			self.ableToHarvest = False



###############################################################################
	def setPlantCallback(self, callback):
		self.plantCallback = callback

###############################################################################
	def draw(self):
		super(Player,self).draw()

		if(self.upgradeImage):
			Globals.screen.blit(self.upgradeImage, self.upgradeImagePos)

		if(self.hasSeedSack):
			pygame.draw.rect(Globals.screen, pygame.Color("Black"), (Globals.screenSize[0] - 50,0,50,25), 0)
			
			#handle vines
			if(self.selectedSeed == 0):
				pygame.draw.rect(Globals.screen, pygame.Color("Red"), (Globals.screenSize[0] - 25,0,25,25), 1)
			else:
				pygame.draw.rect(Globals.screen, pygame.Color("Yellow"), (Globals.screenSize[0] - 25,0,25,25), 1)
			
			self.vinesThumbnailPos.topleft = (Globals.screenSize[0] - 25,0)
			Globals.screen.blit(self.vinesThumbnail, self.vinesThumbnailPos)

			renderLabel = self.font.render(str(self.numVines), 1, pygame.Color("White"))
			labelRect = renderLabel.get_rect()
			labelRect.bottomleft = (Globals.screenSize[0] - 24,24)
			Globals.screen.blit(renderLabel, labelRect)

			#handle portals
			if(self.selectedSeed == 1):
				pygame.draw.rect(Globals.screen, pygame.Color("Red"), (Globals.screenSize[0] - 50,0,25,25), 1)
			else:
				pygame.draw.rect(Globals.screen, pygame.Color("Yellow"), (Globals.screenSize[0] - 50,0,25,25), 1)

			self.portalsThumbnailPos.topleft = (Globals.screenSize[0] - 50,0)
			Globals.screen.blit(self.portalsThumbnail, self.portalsThumbnailPos)

			renderLabel = self.font.render(str(self.numPortals), 1, pygame.Color("White"))
			labelRect = renderLabel.get_rect()
			labelRect.bottomleft = (Globals.screenSize[0] - 48,24)
			Globals.screen.blit(renderLabel, labelRect)

###############################################################################
	def kill(self):
		self.dead = True
		self.image = pygame.image.load("assets/sprites/GardenerDead.png")
		newPos = copy.deepcopy(self.position)
		self.position = self.image.get_rect()
		self.position = newPos
		Globals.achivements.unlockLemming()

###############################################################################
	def handleFalling(self, collisionCallback):
		distance = self.fallingSpeed * (Globals.currentTime - self.lastTick)

		#should look to tidy this, Im doing the same collision detection a
		#bunch of times.
		if(self.shouldFall(collisionCallback)):
			if(self.startedFallingAt == None):
				self.startedFallingAt = copy.deepcopy(self.position)
			
			newPosition = copy.deepcopy(self.position)
			newPosition.bottom += distance

			collidesWith = collisionCallback(newPosition, self.image)
			collided = False
			for item in collidesWith:
				if(type(item).__name__ == "GrassTile" or 
				type(item).__name__ == "SoilTile" or 
				type(item).__name__ == "VineTile"):
					self.position.bottom = item.position.top
					collided = True

			if(not collided):
				self.position.bottom += distance


		elif(not self.startedFallingAt == None):
			if(self.fallWasLethal()):
				print("you died")
			self.startedFallingAt = None

###############################################################################
	def moveLeft(self, collisionCallback):
		distanceToMove = self.speedPerSecond * (Globals.currentTime - self.lastTick)
		self.imageFlipped = True

		newPosition = copy.deepcopy(self.position)
		newPosition.left -= 5
		#newPosition.top -= 2

		if(self.checkForSidewaysMovement(collisionCallback, newPosition)):
			if(Globals.currentTime > self.lastTransition + self.timeBetweenFrames):
				self.lastTransition = Globals.currentTime
				self.animationPos += 1
				if(self.animationPos == len(self.walkingAnimation)):
					self.animationPos = 0
				
				self.image = pygame.transform.flip(self.walkingAnimation[self.animationPos], True, False)

			if(self.position.left - distanceToMove < 0):
				return
			else:
				self.position.left -= distanceToMove
				self.direction = GameObject.left

###############################################################################
	def moveRight(self, collisionCallback):
		distanceToMove = self.speedPerSecond * (Globals.currentTime - self.lastTick)
		self.imageFlipped = False

		newPosition = copy.deepcopy(self.position)
		newPosition.left += 5
		#newPosition.top += 2

		if(self.checkForSidewaysMovement(collisionCallback, newPosition)):
			if(Globals.currentTime > self.lastTransition + self.timeBetweenFrames):
				self.lastTransition = Globals.currentTime
				self.animationPos += 1
				if(self.animationPos >= len(self.walkingAnimation)):
					self.animationPos = 0
				
				self.image = self.walkingAnimation[self.animationPos]

			if(self.position.right + distanceToMove > Globals.screenSize[0]):
				return
			else:
				self.position.left += distanceToMove
				self.direction = GameObject.right

###############################################################################
	def moveUp(self, collisionCallback):
		distanceToMove = self.speedPerSecond * (Globals.currentTime - self.lastTick)
		newPosition = copy.deepcopy(self.position)
		newPosition.top -= 5



		if(self.checkForVerticalMovement(collisionCallback, newPosition)):
			if(not self.climbChannel or not self.climbChannel.get_busy()):
				self.climbChannel = self.climbSound.play()
			
			self.position.top -= distanceToMove

###############################################################################
	def moveDown(self, collisionCallback):
		distanceToMove = self.speedPerSecond * (Globals.currentTime - self.lastTick)
		newPosition = copy.deepcopy(self.position)
		newPosition.top += 5

		if(self.checkForVerticalMovement(collisionCallback, newPosition)):
			if(not self.climbChannel or not self.climbChannel.get_busy()):
				self.climbChannel = self.climbSound.play()
			
			self.position.top += distanceToMove

###############################################################################
	def checkForVerticalMovement(self, collisionCallback, rectToCheck):
		onVine = False
		onGroundTile = False

		collidesWith = collisionCallback(self.position, self.image)
		willCollideWith = collisionCallback(rectToCheck, self.image)
		for collision in itertools.chain(collidesWith, willCollideWith):
			if(type(collision).__name__ == "VineTile"):
				onVine = True
			if(type(collision).__name__ == "GrassTile" or type(collision).__name__ == "SoilTile" ):
				onGroundTile = True
			if(type(collision).__name__ == "PortalTile"):
				collision.moveToOtherPortal(self)

		if(onVine and not onGroundTile):
			return True
		else:
			return False

###############################################################################
	def checkForSidewaysMovement(self, collisionCallback, rectToCheck):
		willCollideWith = collisionCallback(rectToCheck, self.image)
		for collision in willCollideWith:
			if(type(collision).__name__ == "GrassTile" or 
				type(collision).__name__ == "SoilTile" ):
				return False

		return True

###############################################################################
	def shouldFall(self, collisionCallback):
		newPosition = copy.deepcopy(self.position)
		newPosition.top += 5

		willCollideWith = collisionCallback(newPosition, self.image)
		for collision in willCollideWith:
			if(type(collision).__name__ == "GrassTile" or 
				type(collision).__name__ == "SoilTile" or 
				type(collision).__name__ == "VineTile"):
				return False

		return True
			
###############################################################################
	def fallWasLethal(self):
		xVal = self.startedFallingAt.left - self.position.left
		yVal = self.startedFallingAt.top - self.position.top

		xSquare = math.pow(xVal, 2)
		ySquare = math.pow(yVal, 2)

		distance = math.sqrt(xSquare + ySquare)

		if(distance >= self.maxSurvivableFall):
			return True

		return False

###############################################################################
	def getUpgrade(self, upgrade):
		self.upgradeImageStarted = Globals.currentTime
		self.handleGetItemImageFinished = False
		self.image = self.hasItemImage

		self.upgradeImage = upgrade.image
		self.upgradeImagePos = upgrade.position
		self.upgradeImagePos.bottom = self.position.top
		self.upgradeImagePos.left = self.position.left

		self.powerUpSound.play()

		if(type(upgrade).__name__ == "SeedSack"):
			self.getSeedSack()
			Globals.playSpeechEffect(self.seedSackSpeech, Globals.currentlyPlaying)
		elif(type(upgrade).__name__ == "Trowel"):
			Globals.playSpeechEffect(self.trowelSpeech, Globals.currentlyPlaying)
			self.getTrowel()

###############################################################################
	def getSeed(self, seed):
		if(seed.seedtype == Seed.vine):
			self.numVines += 1
			self.pickupSeedSound.play()
		if(seed.seedtype == Seed.portal):
			self.numPortals += 1
			self.pickupSeedSound.play()

###############################################################################
	def getSeedSack(self):
		self.hasSeedSack = True
		self.numVines += 1

###############################################################################
	def getTrowel(self):
		self.hasTrowel = True

###############################################################################
	def handleEvent(self, event):
		if(event.type == pygame.MOUSEBUTTONUP):
			if(event.button == 1):
				self.ableToPlant = True
			elif(event.button == 2):
				self.ableToHarvest = True
		elif(event.type == pygame.KEYUP):
			if(event.key == pygame.K_SPACE):
				self.ableToPlant = True
			elif(event.key == pygame.K_LCTRL):
				self.ableToHarvest = True
			elif(event.key == pygame.K_q or event.key == pygame.K_e):
				self.ableToChangeSeed  = True