import pygame
import Globals

class GameObject(object):
	left = 0
	right = 1
###############################################################################
	def __init__(self, image, position):
		self.image = image
		self.position = position

###############################################################################
	def draw(self):
		Globals.screen.blit(self.image, self.position)

###############################################################################
	def update(self):
		#there is no general case for update, but we dont want the game to crash
		#calling it on objects that dont have it, also dont want to have to keep
		#creating this dummy on each child that does nothing on update.
		pass