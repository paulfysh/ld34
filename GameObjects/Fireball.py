import pygame
import Globals
import copy

from GameObjects.GameObject import GameObject

###############################################################################
class Fireball(GameObject):
	def __init__(self, position, directionModifier, creator):
		self.image = pygame.image.load("assets/sprites/fireball.png").convert_alpha()
		self.position = self.image.get_rect()
		self.position.topleft = position
		super(Fireball, self).__init__(self.image, self.position)

		self.speedPerSecond = 300
		self.lastTick = Globals.currentTime

		self.directionModifier = directionModifier

		self.destroyed = False

		self.lastPortaled = Globals.currentTime - 10
		self.portalCooldown = 5

		self.creator = creator

		self.fireballSound = pygame.mixer.Sound("assets/sounds/fireball.wav")
		self.fireballSound.play()


###############################################################################
	def update(self):
		if(not self.destroyed):
			distanceToMove = self.speedPerSecond * (Globals.currentTime - self.lastTick)
			distanceToMove *= self.directionModifier

			self.position.left += distanceToMove

			self.lastTick = Globals.currentTime

###############################################################################
	def setDirectionModifider(self, facing):
		self.directionModifier = 1

		if(facing == GameObject.left):
			self.directionModifier = -1

###############################################################################
	def destroy(self):
		self.destroyed = True


