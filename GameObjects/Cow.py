import pygame
import Globals
import copy

from GameObjects.GameObject import GameObject

class Cow(GameObject):
###############################################################################
	def __init__(self, position):
		self.walkingAnimation = []
		self.walkingAnimation.append(pygame.image.load("assets/sprites/cow1.png").convert_alpha())
		self.walkingAnimation.append(pygame.image.load("assets/sprites/cow1Walking.png").convert_alpha())
		
		self.exclamationMark = pygame.image.load("assets/sprites/exclamation.png").convert_alpha()
		self.exclamationPos = self.exclamationMark.get_rect()
		self.exclamationImage = self.exclamationMark

		self.eatingImage = pygame.image.load("assets/sprites/cow1Eating.png").convert_alpha()

		self.excitedMooSound = pygame.mixer.Sound("assets/sounds/excitedMoo.wav")
		self.sadMooSound = pygame.mixer.Sound("assets/sounds/sadMoo.wav")

		self.lastTransition = Globals.currentTime
		self.animationPos = 0
		self.timeBetweenFrames = 0.2

		self.image = self.walkingAnimation[self.animationPos]
		self.position = self.image.get_rect()
		self.position.topleft = (position[0], position[1] - 25)

		self.goingRight = True
		self.image = pygame.transform.flip(self.image, True, False)

		self.speedPerSecond = 200
		self.lastTick = Globals.currentTime
		self.imageFlipped = False

		self.destination = None
		self.newDestinationDuration = 0.5
		self.gotDestinationAt = None
		self.newDestination = False

		self.eating = False
		self.startedEating = None
		self.eatingDuration = 1
		self.eatingTransition = 0.1
		self.lastEatingTransition = Globals.currentTime
		self.showingEatingImage = False

		self.startedFallingAt = None

		self.lastPortaled = Globals.currentTime
		self.portalCooldown = 5

		self.dead = False

###############################################################################
	def update(self, collisionCallback):
		if(not self.dead):
			if(self.newDestination):
				self.newDestination = False
				self.exclamationPos.bottom = (self.position.top)
				if(self.goingRight):
					self.exclamationImage = pygame.transform.flip(self.exclamationMark, True, False)
					self.exclamationPos.right = self.position.right 
					self.image = pygame.transform.flip(self.walkingAnimation[0], True, False)
				else:
					self.exclamationImage = self.exclamationMark
					self.exclamationPos.left = self.position.left
					self.image = self.walkingAnimation[0]
				
			
			if(not self.destination):
				if(not self.eating):
					self.move(collisionCallback)
					self.lookForPlant(collisionCallback)
				else:
					if(Globals.currentTime >= self.lastEatingTransition + self.eatingTransition):
						self.lastEatingTransition = Globals.currentTime
						if(self.showingEatingImage):
							self.image = self.walkingAnimation[0]
							self.showingEatingImage = False
						else:
							self.image = self.eatingImage
							self.showingEatingImage = True
						if(self.goingRight):
							self.image = pygame.transform.flip(self.image, True, False)
					if(Globals.currentTime >= self.startedEating + self.eatingDuration):
						self.eating = False

			elif(Globals.currentTime >= self.gotDestinationAt + self.newDestinationDuration):
				self.move(collisionCallback)
				
			self.lastTick = Globals.currentTime

###############################################################################
	def kill(self):
		self.dead = True
		self.image = pygame.image.load("assets/sprites/cow1Dead.png")
		newPos = copy.deepcopy(self.position)
		self.position = self.image.get_rect()
		self.position = newPos
		self.sadMooSound.play()

		Globals.achivements.unlockBurgerChef()
		Globals.achivements.cowsKilled += 1

###############################################################################
	def draw(self):
		super(Cow, self).draw()
		if(self.destination and
		Globals.currentTime < self.gotDestinationAt + self.newDestinationDuration):
			Globals.screen.blit(self.exclamationImage, self.exclamationPos)

###############################################################################
	def move(self, collisionCallback):
		if(not self.startedFallingAt):
			testRect = copy.deepcopy(self.position)
			distanceToMove = self.speedPerSecond * (Globals.currentTime - self.lastTick)
			
			if(self.goingRight):
				testRect.left += distanceToMove
			else:
				testRect.left -= distanceToMove

			#testRect.bottom  += 1 #(check for ground under new position)
			hasGround = False
			hasBlocker = False

			if(testRect.right < Globals.screenSize[0] and testRect.left > 0):
				collisions = collisionCallback(testRect)

				for collision in collisions:
					if(type(collision).__name__ == "GrassTile" or 
						type(collision).__name__ == "SoilTile"):
							hasBlocker = True

				if(self.destination):
					'''cows suffer from tunnel vision, they fall off cliffs in their
					pursuit of food'''
					hasGround = True
				else:
					testRect.bottom += 1
					collisions = collisionCallback(testRect)
					for collision in collisions:
						if(type(collision).__name__ == "GrassTile" or 
							type(collision).__name__ == "SoilTile"):
								if(self.goingRight):
									if(collision.position.right > self.position.right):
										hasGround = True
								else:
									if(collision.position.left < self.position.left):
										hasGround = True

			if(hasGround and not hasBlocker):
				self.position.left = testRect.left
				self.updateWalkImage()
			else:
				self.flipImage()
				self.goingRight = not self.goingRight



###############################################################################
	def flipImage(self):
		'''called when the cow turns around, happy just to have the stationary image'''
		if(self.goingRight):
			self.image = pygame.transform.flip(self.walkingAnimation[0], True, False)
		else:
			self.image = self.walkingAnimation[0]

###############################################################################
	def updateWalkImage(self):
		if(Globals.currentTime > self.lastTransition + self.timeBetweenFrames):
				self.lastTransition = Globals.currentTime
				self.animationPos += 1
				if(self.animationPos == len(self.walkingAnimation)):
					self.animationPos = 0
				
				if(self.goingRight):
					self.image = pygame.transform.flip(self.walkingAnimation[self.animationPos], True, False)
				else:
					self.image = self.walkingAnimation[self.animationPos]

###############################################################################
	def lookForPlant(self, collisionCallback):
		if(self.goingRight):
			visionRect = pygame.Rect(self.position.center[0], self.position.center[1], Globals.screenSize[0] - self.position.center[0], 5)
		else:
			visionRect = pygame.Rect(0, self.position.center[1], self.position.center[0], 5)

		collisions = collisionCallback(visionRect)
		for collision in collisions:
			if(type(collision).__name__ == "VineTile"):
				self.destination = collision.position
				self.newDestination = True
				self.gotDestinationAt = Globals.currentTime

		if(self.destination):
			for collision in collisions:
				if(type(collision).__name__ == "GrassTile" or 
					type(collision).__name__ == "SoilTile"):
						if(self.goingRight):
							if(self.destination.left > collision.position.left):
								self.destination = None
								self.newDestination = False
								self.gotDestinationAt = None
								return
						else:
							if(self.destination.left < collision.position.left):
								self.destination = None
								self.newDestination = False
								self.gotDestinationAt = None
								return
		if(self.destination):
			self.excitedMooSound.set_volume(0.5)
			self.excitedMooSound.play()

###############################################################################
	def eatPlant(self, plant):
		plant.destroy(True)
		self.destination = None
		self.newDestination = False
		self.gotDestinationAt = None

		self.eating = True
		self.startedEating = Globals.currentTime