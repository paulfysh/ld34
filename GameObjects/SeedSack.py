import pygame

from GameObjects.GameObject import GameObject


class SeedSack(GameObject):
###############################################################################
	def __init__(self, position):
		self.image = pygame.image.load("assets/sprites/seedSack.png").convert_alpha()
		self.position = self.image.get_rect()
		self.position.topleft = position
		super(SeedSack, self).__init__(self.image, self.position)