import sys
import Globals
import pygame
from menus.GenericMenu import GenericMenu

class InGameMenu(GenericMenu):

###############################################################################
	def __init__(self, currentScreen, levelParser):
		super(InGameMenu, self).__init__()

		self.currentScreen = currentScreen
		self.levelParser = levelParser

		self.addMenuOption("Restart Level", self.restartLevel)
		self.addMenuOption("Continue", self.resume)
		self.addMenuOption("Quit", self.quit)

###############################################################################
	def draw(self):
		self.currentScreen.draw()

		#draw a black box
		width = 0
		left = 0
		height = 0
		top = 0

		singleStringHeight = 0

		for item in self.menuOptions:
			itemSize = self.font.size(item[0])
			if(itemSize[0] > width):
				width = itemSize[0]
			
			height += itemSize[1] + 2
			singleStringHeight = itemSize[1] + 2

		#give some extra space
		height += 10
		width += 10

		left = (Globals.screenSize[0] / 2) - (width / 2)
		top = (Globals.screenSize[1] / 2)

		pygame.draw.rect(Globals.screen, pygame.Color("Black"), (left, top, width, height), 0)


		super(InGameMenu, self).draw()

###############################################################################
	def restartLevel(self):
		currentLevelNo = self.currentScreen.levelNo
		currentPlayer = self.currentScreen.player
		self.nextScreen = self.levelParser.parseLevel(currentLevelNo, currentPlayer)

		currentPlayer.resetForNextLevel()
		if(currentLevelNo == 1):
			#unfortunately have to do some level specific stuff here because of item aquisition
			currentPlayer.hasSeedSack = False
		elif(currentLevelNo == 2):
			currentPlayer.numVines = 0


###############################################################################
	def quit(self):
		sys.exit()

###############################################################################
	def resume(self):
		self.nextScreen = self.currentScreen
		self.currentScreen.nextScreen = None