import sys
import Globals
import pygame
from menus.GenericMenu import GenericMenu
from levels.LevelParser import LevelParser

class MainMenu(GenericMenu):

###############################################################################
	def __init__(self):
		super(MainMenu, self).__init__()
		self.addMenuOption("New Game", self.createLevel1)
		self.addMenuOption("Quit", self.callback_function)

		self.background = pygame.image.load("assets/sprites/background.png").convert_alpha()
		self.backgroundRect = self.background.get_rect().topleft = (0,0)

		self.groundTile = pygame.image.load("assets/sprites/grassTile.png").convert_alpha()
		self.cow = pygame.image.load("assets/sprites/cow1.png").convert_alpha()
		#self.cow = pygame.transform.flip(self.cow, True, False)
		self.cowrect = self.cow.get_rect()
		self.cowrect.bottom = Globals.screenSize[1] - self.groundTile.get_rect().height
		self.cowrect.right = Globals.screenSize[0]

		self.parser = LevelParser()
		self.nextScreen = None

		self.logo = pygame.image.load("assets/sprites/logo.png").convert_alpha()
		self.logoPos = self.logo.get_rect()
		self.logoPos.center = (Globals.screenSize[0] / 2, Globals.screenSize[1] * 0.25)
		


###############################################################################
	def callback_function(self):
		sys.exit()

###############################################################################
	def createLevel1(self):
		#TODO - NO REALLY CHANGE THIS BACK TO LEVEL 1
		self.nextScreen = self.parser.parseLevel(1, None)

###############################################################################
	def draw(self):
		Globals.screen.blit(self.background, self.backgroundRect)

		currentGroundTileRect = self.groundTile.get_rect()
		currentGroundTileRect.top = Globals.screenSize[1] - currentGroundTileRect.height
		currentGroundTileRect.left = 0

		while currentGroundTileRect.left < Globals.screenSize[0]:
			Globals.screen.blit(self.groundTile, currentGroundTileRect)
			currentGroundTileRect.left += currentGroundTileRect.width


		Globals.screen.blit(self.cow, self.cowrect)

		Globals.screen.blit(self.logo, self.logoPos)



		super(MainMenu, self).draw()