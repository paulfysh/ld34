import pygame
import Globals

from GameObjects.GameScreen import GameScreen

class GenericMenu(GameScreen):

###############################################################################
	def __init__(self):
		self.font = pygame.font.SysFont("monospace", 15)
		self.titleFont = pygame.font.SysFont("monospace", 30)

		self.lastAddedBottom = (Globals.screenSize[1] / 2)
		self.menuOptions = []
		self.currentSelection = 0

		self.nextScreen = None

		self.newKeyPress = True

###############################################################################
#########################Menu Setup############################################
###############################################################################
	def addMenuOption(self, text, callback):
		width, height = self.font.size(text)
		center = ( (Globals.screenSize[0] / 2 ), self.lastAddedBottom + (height / 2) )
		self.menuOptions.append( (text, callback, center) )
		self.lastAddedBottom = self.lastAddedBottom + height + 2

###############################################################################
###########################Render##############################################
###############################################################################
	def draw(self):
		currentOptionNo = 0
		for option in self.menuOptions:
			colourToUse = pygame.Color("Yellow")
			if(currentOptionNo == self.currentSelection):
				colourToUse = pygame.Color("Red")

			renderLabel = self.font.render(option[0], 1, colourToUse)
			rect = renderLabel.get_rect()
			rect.center = option[2]
			Globals.screen.blit(renderLabel, rect)

			currentOptionNo += 1

	def update(self):
		#must be implemented for screen manager, but we dont care here, so just pass
		#should probably derrive from a gamescreen base class that just goggles. 
		pass

###############################################################################
#########################Handle Input##########################################
###############################################################################
	def handleEvent(self, event):
		if(event.type == pygame.KEYDOWN):
			self.handleKeydown(event)
		elif(event.type == pygame.KEYUP):
			self.handleKeyup(event)

###############################################################################
	def handleKeydown(self, event):
		if(self.newKeyPress):
			if(event.key == pygame.K_DOWN):
				if( self.currentSelection + 1 > (len(self.menuOptions) - 1) ):
					self.currentSelection = 0
				else:
					self.currentSelection += 1

			elif(event.key == pygame.K_UP):
				if( self.currentSelection - 1 >= 0 ):
					self.currentSelection -= 1
				else:
					self.currentSelection = (len(self.menuOptions) - 1)
			elif(event.key == pygame.K_RETURN):
				self.menuOptions[self.currentSelection][1]()

		self.newKeyPress = False


###############################################################################
	def handleKeyup(self, event):
		self.newKeyPress = True

