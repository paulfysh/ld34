from menus.MainMenu import MainMenu

###############################################################################
class ScreenManager():

###############################################################################
	def __init__(self):
		self.currentScreen = MainMenu()

###############################################################################
######################Screen management########################################
###############################################################################
#all screens MUST implement draw, handleEvent and update
	def draw(self):
		if(self.currentScreen):
			self.currentScreen.draw()

###############################################################################
	def handleEvent(self, event):
		if(self.currentScreen):
			self.currentScreen.handleEvent(event)

###############################################################################
	def update(self):
		if(self.currentScreen):
			self.currentScreen.update()
			if(self.currentScreen.nextScreen):
				self.currentScreen = self.currentScreen.nextScreen

