import pygame
import Globals

###############################################################################
class Achivements(object):
	def __init__(self):
		self.greenFingerUnlocked = False
		self.cowTippingUnlocked = False
		self.lemmingUnlocked = False
		self.pacifistUnlocked = False
		self.thinkingWithPortalsUnlocked = False
		self.NBGUnlocked = False
		self.burgerChefUnlocked = False

		self.cowsKilled = 0

		self.lastUnlock = Globals.currentTime - 3
		self.displayFor = 3

		self.achiTitle = []
		self.achiText = []

		self.font = pygame.font.SysFont("monospace", 15)

###############################################################################
	def unlockGreenFinger(self):
		'''beat level 1'''
		if(not self.greenFingerUnlocked):
			self.greenFingerUnlocked = True

			self.achiTitle.append("Greenfinger")
			self.achiText.append("You beat the first level, well done!")
			self.lastUnlock = Globals.currentTime

###############################################################################
	def unlockCowTipping(self):
		'''drop a cow off a ledge with a portal'''
		if(not self.cowTippingUnlocked):
			self.cowTippingUnlocked = True

			self.achiTitle.append("Cow Tipping")
			self.achiText.append("You sent a cow over the edge, must have made you mad")
			self.lastUnlock = Globals.currentTime

###############################################################################
	def unlockLemming(self):
		'''kill yourself'''
		if(not self.lemmingUnlocked):
			self.lemmingUnlocked = True

			self.achiTitle.append("Lemming")
			self.achiText.append("Having a bad day?")
			self.lastUnlock = Globals.currentTime

###############################################################################
	def unlockPacifist(self):
		'''dont ever kill a cow and beat the game'''
		if(not self.pacifistUnlocked):
			self.pacifistUnlocked = True

			self.achiTitle.append("Pacifist")
			self.achiText.append("You didn't kill a single cow, good show!")
			self.lastUnlock = Globals.currentTime

###############################################################################
	def unlockThinkingWithPortals(self):
		'''beat level 4'''
		if(not self.thinkingWithPortalsUnlocked):	
			self.thinkingWithPortalsUnlocked = True

			self.achiTitle.append("Thinking with portals")
			self.achiText.append("Had to get this one in...")
			self.lastUnlock = Globals.currentTime

###############################################################################
	def unlockNaturalBornGrower(self):
		'''beat the game'''
		if(not self.NBGUnlocked):
			self.NBGUnlocked = True

			self.achiTitle.append("Natural Born Grower")
			self.achiText.append("You stuck with it and beat the game, Im impressed")
			self.lastUnlock = Globals.currentTime

###############################################################################
	def unlockBurgerChef(self):
		'''kill a cow'''
		if(not self.burgerChefUnlocked):
			self.burgerChefUnlocked = True

			self.achiTitle.append("Burger Chef")
			self.achiText.append("Its ok, I hate them too, cudd chewing jerks")
			self.lastUnlock = Globals.currentTime

###############################################################################
	def draw(self):
		if(Globals.currentTime < self.lastUnlock + self.displayFor):
			pos = 0
			lastY = 0
			for title in self.achiTitle:
				pygame.draw.rect(Globals.screen, pygame.Color("Black"), (Globals.screenSize[0] * 0.25, lastY, Globals.screenSize[0] * 0.5, 100), 0)
				pygame.draw.rect(Globals.screen, pygame.Color("Yellow"), (Globals.screenSize[0] * 0.25, lastY, Globals.screenSize[0] * 0.5, 100), 1)
				pygame.draw.rect(Globals.screen, pygame.Color("Yellow"), (Globals.screenSize[0] * 0.25, lastY, Globals.screenSize[0] * 0.5, 50), 1)

				renderLabel = self.font.render("Achivement: " + title, 1, pygame.Color("Yellow"))
				rect = renderLabel.get_rect()
				rect.topleft = ((Globals.screenSize[0] * 0.25) + 5, lastY + 5)
				Globals.screen.blit(renderLabel, rect)

				renderLabel = self.font.render(self.achiText[pos], 1, pygame.Color("Yellow"))
				rect  = renderLabel.get_rect()
				rect.topleft = ((Globals.screenSize[0] * 0.25) + 5, lastY + 55)
				Globals.screen.blit(renderLabel, rect)
				lastY = lastY + 105
				pos += 1
		else:
			self.achiTitle = []
			self.achiText = []


